var Bubble = require("./model/bubbleModel").Bubble;
var Board = require("./model/boardModel").Board;
var Cannon = require("./model/cannonModel");
var Settings = require("./model/Settings");

var container = document.createElement("div");
container.className = "main-view";
document.body.appendChild(container);

var boardModel = new Board()
  .setSize(Settings.ROWS, Settings.COLUMNS, Settings.BUBBLE_SIZE)
  //.createPredefined();
  .createRandom(Settings.INITIAL_ROWS, Settings.COLUMNS)

var cannonModel = new Cannon();
cannonModel.setPosition(Settings.CANNON_POSITION_X, Settings.CANNON_POSITION_Y);
cannonModel.setBoardSize(boardModel.getRows(),
  boardModel.getColumns(), boardModel.getItemSize());
cannonModel.load();
cannonModel.fire();

var pepjs = require("pepjs/dist/pep");
var BoardView = require("./view/boardView");
var boardView = new BoardView();
var boardContainer = document.createElement("div");
container.appendChild(boardContainer);
boardView.setModel(boardModel);
boardView.setContainer(boardContainer);
boardView.draw();

var CannonView = require("./view/cannonView");
var cannonView = new CannonView();
cannonView.setModel(cannonModel);
cannonView.draw();
container.appendChild(cannonView.element);

var CollisionDetector = require("./controller/CollisionDetector");
var collisionDetector = new CollisionDetector();
collisionDetector.setBoardModel(boardModel);
collisionDetector.setCannonModel(cannonModel);

var GameLoop = require("./controller/gameLoop");
GameLoop.run();
GameLoop.addItem(cannonView);
GameLoop.addItem(collisionDetector);
