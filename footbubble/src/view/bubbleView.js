var BubbleView = function() {
  this.model = null;
  this.element = document.createElement("div");
}

BubbleView.prototype.getClassName = function() {
  return "bubble-view";
}

BubbleView.prototype.setModel = function(model) {
  this.model = model;
}

BubbleView.prototype.draw = function() {
  this.element.className = this.getClassName() + " bubble-type-" + this.model.getType();
  this.move(this.model.getX(), this.model.getY());
  this.element.style.width = this.model.getSize() + "px";
  this.element.style.height = this.model.getSize() + "px";
  this.element.setAttribute("data-id", this.model.id);
}

BubbleView.prototype.move = function(x, y) {
  var translate = "translate({x}px, {y}px)"
    .replace("{x}", x)
    .replace("{y}", y);
  this.element.style.transform = translate;
}

module.exports = BubbleView;
