var BubbleView = require("./bubbleView");
var _ = require("underscore");
var GameLoop = require("./../controller/gameLoop");
var Settings = require("./../model/Settings");

var FlyingBubbleView = function() {
  BubbleView.call(this);
}

FlyingBubbleView.prototype = _.extend({}, BubbleView.prototype);

FlyingBubbleView.prototype.getClassName = function() {
  return "bubble-view flying-bubble-view";
}

FlyingBubbleView.prototype.update = function(timeElapsed) {
  var speed = Settings.BUBBLE_SPEED;
  var dx = this.model.velocity.x * speed * timeElapsed/1000;
  var dy = this.model.velocity.y * speed * timeElapsed/1000;

  this.model.move(dx, dy);
  this.move(this.model.x, this.model.y);
}

FlyingBubbleView.prototype.addToGameLoop = function(velocityVector) {
  this.model.setVelocity(velocityVector);

  GameLoop.addItem(this);
}

module.exports = FlyingBubbleView;
