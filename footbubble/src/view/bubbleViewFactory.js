var BubbleView = require("./bubbleView");
var FlyingBubbleView = require("./flyingBubbleView");

var BubbleViewFactory = function() {};

BubbleViewFactory.prototype.create = function(model) {
  return setupBubble(new BubbleView(), model);
}

BubbleViewFactory.prototype.createFlying = function(model) {
  return setupBubble(new FlyingBubbleView(), model);
}

function setupBubble(view, model) {
  view.setModel(model);
  view.draw();
  return view;
}

module.exports = BubbleViewFactory;
