var BubbleViewFactory = require("./bubbleViewFactory");
var factory = new BubbleViewFactory();
var _ = require("underscore");
var Victor = require("victor");

var CannonView = function() {
  this.model = null;
  this.bubbleView = null;
  this.items = [];

  var element = document.createElement("div");
  this.element = element;
  element.className = "cannon-view";

  element.addEventListener("pointermove", this.onPointerMove.bind(this));
  element.addEventListener("pointerup", this.onPointerUp.bind(this));
}

CannonView.prototype.onPointerMove = function(e) {
  this.velocityVector = calculateVelocityVector(this.bubbleModel, e);
}

CannonView.prototype.onPointerUp = function(e) {
  var flyingBubbleView = factory.createFlying(this.model.getCurrent());
  this.element.appendChild(flyingBubbleView.element);
  flyingBubbleView.addToGameLoop(_.extend({}, this.velocityVector));

  this.items.push(flyingBubbleView);

  this.element.removeChild(this.bubbleView.element);
  this.bubbleView = null;

  this.model.fire();
  this.bubbleModel = this.model.getCurrent();

  this.load();
}

CannonView.prototype.setModel = function(model) {
  this.model = model;
  this.element.style.width = model.boardWidth + "px";
  this.element.style.height = model.boardHeight + "px";
  var binding = this.model.updated.add(this.onModelUpdated);
  binding.context = this;
}

CannonView.prototype.onModelUpdated = function(bubble) {
  var view = _.find(this.items, function(item){
    return item.model == bubble;
  });
  if (view) view.element.parentNode.removeChild(view.element);
}

CannonView.prototype.draw = function() {
  this.bubbleModel = this.model.getCurrent();

  this.load();

  var arrowElement = document.createElement("div");
  this.arrowElement = arrowElement;
  this.arrowElement.className = "arrow-view";
  this.element.appendChild(arrowElement);
}

CannonView.prototype.load = function() {
  var bubbleView = factory.create(this.bubbleModel);
  this.bubbleView = bubbleView;
  this.element.appendChild(bubbleView.element);
}

CannonView.prototype.update = function(timeElapsed) {
  if (this.velocityVector) {
    // Redraw cannon to show current angle.
    var angle = Math.atan2(this.velocityVector.y, this.velocityVector.x)
      * (180 / Math.PI) + 90;
    var transform = "rotate({x}deg)".replace("{x}", angle);
    this.arrowElement.style.transform = transform;
  }
}

function calculateVelocityVector(bubble, event) {
  var x1 = bubble.x + bubble.size / 2;
  var y1 = bubble.y + bubble.size / 2;
  var x2 = event.clientX;
  var y2 = event.clientY;
  return new Victor(x2 - x1, y2 - y1).normalize();
}

module.exports = CannonView;
