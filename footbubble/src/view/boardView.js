var BubbleViewFactory = require("./bubbleViewFactory");
var EmptyBubble = require("./../model/bubbleModel").EmptyBubble;
var factory = new BubbleViewFactory();

var BoardView = function() {}

BoardView.prototype.setModel = function(model) {
    this.model = model;
    this.itemsByID = {};

    var binding = this.model.updated.add(this.onModelUpdated);
    binding.context = this;
}

BoardView.prototype.onPointerUp = function(event) {
  var id = event.target.getAttribute("data-id");
  if (this.itemsByID[id]) {
    var bubbleView = this.itemsByID[id];
    var siblings = this.model.getAllColoredSiblings(bubbleView.model);

    /*// Remove bubbles from the board.
    siblings.forEach((function(bubble){
      this.model.removeBubble(bubble);
    }).bind(this));*/
  }
}

BoardView.prototype.onModelUpdated = function(bubble) {
  this.clear();
  this.draw();
}

BoardView.prototype.setContainer = function(container) {
  container.className = "board-view";
  this.container = container;
  this.container.addEventListener("pointerup", this.onPointerUp.bind(this));
}

BoardView.prototype.clear = function() {
  this.container.innerHTML = "";
}

BoardView.prototype.draw = function() {
  this.drawBubbles();
}

BoardView.prototype.drawBubbles = function() {
  for (var i = 0; i < this.model.items.length; i++) {
    var row = this.model.items[i];
    for (var j = 0; j < row.length; j++) {
      var model = this.model.items[i][j];
      if (model == EmptyBubble)
        continue;

      var view = factory.create(model);
      this.itemsByID[model.id] = view;
      this.container.appendChild(view.element);
    }
  }
}

module.exports = BoardView;
