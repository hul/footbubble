var Signal = require("signals");
var BubbleFactory = require("./bubbleFactory");
var EmptyBubble = require("./bubbleModel").EmptyBubble;
var factory = new BubbleFactory();
var _ = require("underscore");

var Board = function() {
  this.rows = 0;
  this.columns = 0;
  this.items = [];
  this.updated = new Signal();
}

Board.prototype.getItemSize = function() {
  return this.itemSize;
}

Board.prototype.getRows = function() {
  return this.rows;
}

Board.prototype.getColumns = function() {
  return this.columns;
}

Board.prototype.getItems = function() {
  return this.items;
}

Board.prototype.moveBubblesDown = function() {
  for (var i = 0; i < this.items.length; i++) {
    var row = this.items[i];
    for (var j = 0; j < row.length; j++) {
      var bubble = row[j];
      bubble.setPositionFromGrid(i + 1, j);
    }
  }
}

Board.prototype.addNewRow = function() {
  this.moveBubblesDown();
  this.items.unshift(createRow(0, this.columns, factory));
  this.height++;
}

Board.prototype.setSize = function(rows, columns, size) {
  this.rows = rows;
  this.columns = columns;
  this.itemSize = size;
  return this;
}

Board.prototype.createRandom = function(rows, columns) {
  for (var i = 0; i < rows; i++) {
    var row = createRow(i, columns, factory);
    this.items.push(row);
  }
  return this;
}

Board.prototype.createPredefined = function() {
  var setup = [
    [1,1,1,1,2,3,3,4,1],
    [1,2,3,2,2,3,3,3,1],
    [2,2,3,3,4,5,5,5,1],
    [2,3,3,2,1,1,1,1,1]
  ];
  for(var i = 0; i<setup.length; i++) {
    var row = [];
    for (var j=0; j<setup[i].length; j++) {
      var item = factory.createRandom({i:i, j:j, type:setup[i][j]});
      row.push(item);
    }
    this.items.push(row);
  }
  return this;
}

Board.prototype.clear = function() {
  this.items = [];
  return this;
}

Board.prototype.removeBubble = function(bubble) {
  this.items[bubble.i][bubble.j] = EmptyBubble;
  this.updated.dispatch(bubble);
}

Board.prototype.getBubble = function(i, j) {
  if (!Array.isArray(this.items[i])) return EmptyBubble;
  if (!this.items[i][j]) return EmptyBubble;
  return this.items[i][j];
}

Board.prototype.getSiblings = function(bubble) {
  var siblings = [];
  var delta = 1;
  siblings.push(this.getBubble(bubble.i, bubble.j - 1));
  siblings.push(this.getBubble(bubble.i, bubble.j + 1));
  if (bubble.i % 2 == 0) {
    siblings.push(this.getBubble(bubble.i - 1, bubble.j - delta));
    siblings.push(this.getBubble(bubble.i - 1, bubble.j));
    siblings.push(this.getBubble(bubble.i + 1, bubble.j - delta));
    siblings.push(this.getBubble(bubble.i + 1, bubble.j));
  }
  else {
    siblings.push(this.getBubble(bubble.i - 1, bubble.j + delta));
    siblings.push(this.getBubble(bubble.i - 1, bubble.j));
    siblings.push(this.getBubble(bubble.i + 1, bubble.j));
    siblings.push(this.getBubble(bubble.i + 1, bubble.j + delta));
  }

  return siblings.filter(function(item){
    return item != EmptyBubble;
  });
}

Board.prototype.getColoredSiblings = function(bubble) {
  return this.getSiblings(bubble).filter(function(sibling){
    return sibling.isTheSameTypeAs(bubble);
  });
}

Board.prototype.getAllColoredSiblings = function(bubble) {
  var visited = [bubble];
  var colorizedSiblings = [];
  var siblings = this.getColoredSiblings(bubble);
  var type = bubble.type;

  while(siblings.length > 0) {
    var current = siblings.shift();
    if (visited.indexOf(current) == -1) visited.push(current);

    var currentSiblings = this.getColoredSiblings(current);
    currentSiblings = currentSiblings.filter((function(item){
        return visited.indexOf(item) == -1;
    }).bind(this));

    siblings = siblings.concat(currentSiblings);
  }

  return visited;
}

function createRow(i, columns, factory) {
  var row = [];
  for (var j = 0; j < columns; j++) {
    var bubble = factory.createRandom({
      i: i,
      j: j
    });
    row.push(bubble);
  }
  return row;
}

module.exports.Board = Board;
