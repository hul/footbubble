var Bubble = require("./bubbleModel").Bubble;
var BubbleType = require("./bubbleModel").BubbleType;
var _ = require("underscore");
var BUBBLE_SIZE = require("./bubbleModel").BUBBLE_SIZE;

var BubbleFactory = function() {};

BubbleFactory.prototype.createRandom = function(options) {
  var bubble = new Bubble();
  var types = _.values(BubbleType);
  if (!options) options = {};

  var type = options.type || types[Math.floor(Math.random() * types.length)];
  bubble.setType(type);

  var size = options.size || BUBBLE_SIZE;
  bubble.setSize(size);

  var i = options.i || 0;
  var j = options.j || 0;
  bubble.setPositionFromGrid(i, j);

  return bubble;
}

module.exports = BubbleFactory;
