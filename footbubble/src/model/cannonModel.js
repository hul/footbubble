var MAX_SLOTS = 5;

var Signal = require("signals");
var BubbleFactory = require("./bubbleFactory");
var factory = new BubbleFactory();
var _ = require("underscore");

var Cannon = function() {
  this.resetAttempts();
  this.reset();
  this.firedItems = [];
  this.updated = new Signal();
};

Cannon.prototype.getFiredItems = function() {
  return this.firedItems;
}

Cannon.prototype.getCurrent = function() {
    return this.ammo[this.ammo.length - 1];
}

Cannon.prototype.setPosition = function(x, y) {
    this.x = x;
    this.y = y;
}

Cannon.prototype.setBoardSize = function(rows, columns, size) {
  this.boardWidth = columns * size + size / 2;
  this.boardHeight = rows * size;
}

Cannon.prototype.removeBubble = function(bubble) {
  var index = this.firedItems.indexOf(bubble);
  if (index != -1) {
    this.firedItems.splice(index, 1);
    this.updated.dispatch(bubble);
  }
}

Cannon.prototype.fire = function() {
  var bullet = this.ammo.pop();
  this.firedItems.push(bullet);
  this.slots--;

  if (this.slots <= 0) {
    this.attempt++;

    if (this.attempt == MAX_SLOTS) {
      this.resetAttempts();
    }

    this.reset();
    this.load();
  }

  return this;
}

Cannon.prototype.load = function() {
  var ammo = [];
  for (var i = 0; i < this.slots; i++) {
      var bubble = factory.createRandom();
      bubble.setPosition(this.x, this.y);
      ammo.push(bubble);
  }
  this.ammo = ammo;
  return this;
}

Cannon.prototype.reset = function() {
  this.ammo = [];
  this.slots = MAX_SLOTS - this.attempt;

  return this;
}

Cannon.prototype.resetAttempts = function() {
  this.attempt = 0;

  return this;
}

module.exports = Cannon;
