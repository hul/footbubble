var _ = require("underscore");
var EmptyBubble = {};

var BubbleType = {
  TYPE_1: 1,
  TYPE_2: 2,
  TYPE_3: 3,
  TYPE_4: 4,
  TYPE_5: 5
};

var BUBBLE_SIZE = 40;

var Bubble = function(){
  this.x = 0;
  this.y = 0;
  this.i = 0;
  this.j = 0;
  this.size = 0;
  this.type = 0;
  this.velocity = null;
  this.isImmuneToCollision = false;
  this.id = _.uniqueId();
}

Bubble.prototype.setPositionFromGrid = function(i, j) {
  var x = j * this.size;
  var y = i * this.size;
  this.i = i;
  this.j = j;
  var offset = (i % 2 == 1? this.size/2 : 0);
  this.x = x + offset;
  this.y = y;
  this.id = i + "-" + j;
  return this;
}

Bubble.prototype.setType = function(type) {
  this.type = type;
  return this;
}

Bubble.prototype.setPosition = function(x, y) {
  this.x = x;
  this.y = y;
  return this;
}

Bubble.prototype.move = function(dx, dy) {
  this.x += dx;
  this.y += dy;
  return this;
}

Bubble.prototype.setSize = function(size) {
  this.size = size;
  return this;
}

Bubble.prototype.setVelocity = function(velocity) {
  this.velocity = velocity.clone();
}

Bubble.prototype.getVelocity = function() {
  return this.velocity;
}

Bubble.prototype.getType = function() {
  return this.type;
}

Bubble.prototype.getX = function() {
  return this.x;
}

Bubble.prototype.getY = function() {
  return this.y;
}

Bubble.prototype.getSize = function() {
  return this.size;
}

Bubble.prototype.isTheSameTypeAs = function(bubble) {
  return bubble.type == this.type;
}

module.exports.BubbleType = BubbleType;
module.exports.Bubble = Bubble;
module.exports.BUBBLE_SIZE = BUBBLE_SIZE;
module.exports.EmptyBubble = EmptyBubble;
