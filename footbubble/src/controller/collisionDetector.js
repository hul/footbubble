var CollisionDetector = function() {};
var Victor = require("victor");
var _ = require("underscore");
var GameLoop = require("./gameLoop");
var EmptyBubble = require("./../model/bubbleModel").EmptyBubble;

CollisionDetector.prototype.setBoardModel = function(boardModel) {
  this.boardModel = boardModel;
  this.top = 0;
  this.left = 0;
  this.right = boardModel.getColumns() * boardModel.getItemSize();
  this.bottom = boardModel.getRows() * boardModel.getItemSize();
  this.size = boardModel.getItemSize();
  this.halfSize = boardModel.getItemSize() / 2;
}

CollisionDetector.prototype.setCannonModel = function(model) {
  this.cannonModel = model;
}

CollisionDetector.prototype.update = function() {
  _.each(this.cannonModel.getFiredItems(), checkCollisions.bind(this));
}

function checkCollisions(bubble) {
  checkCollisionWithBoard.call(this, bubble);
  checkCollisionWithBubbles.call(this, bubble);
}

function checkCollisionWithBubbles(bubble) {
  _.each(this.boardModel.getItems(), (function(row) {
    _.each(row, (function(boardBubble) {
      if (boardBubble == EmptyBubble || bubble.isImmuneToCollision)
        return;
      var x1 = bubble.getX();
      var y1 = bubble.getY();
      var x2 = boardBubble.getX();
      var y2 = boardBubble.getY();
      var v = new Victor(x2 - x1, y2 - y1);
      if (v.length() <= this.size) {
        handleCollision.call(this, bubble, boardBubble);
      }
    }).bind(this));
  }).bind(this));
}

function handleCollision(bubble, boardBubble) {
  bubble.isImmuneToCollision = true;
  bubble.setVelocity(new Victor(0,0));
  GameLoop.removeItem(bubble);

  // TODO:
  //  - add bubble to free slot of the boardBubble on the board

  var v = new Victor(boardBubble.x - bubble.x, boardBubble.y - bubble.y);
  //this.boardModel.removeBubble(boardBubble);
  this.cannonModel.removeBubble(bubble);
  console.log("hit", bubble.type, boardBubble.id, v.angleDeg(), v.verticalAngleDeg());
}

function checkCollisionWithBoard(bubble) {
  var velocity = bubble.getVelocity();

  if (bubble.getX() < this.left) {
    bubble.setVelocity(new Victor(Math.abs(velocity.x), velocity.y));
  }
  else if (bubble.getX() + this.halfSize > this.right) {
    bubble.setVelocity(new Victor(-Math.abs(velocity.x), velocity.y));
  }
  if (bubble.getY() < this.top) {
    bubble.setVelocity(new Victor(velocity.x, Math.abs(velocity.y)));
  }
  else if (bubble.getY() + 2*this.halfSize > this.bottom) {
    bubble.setVelocity(new Victor(velocity.x, -Math.abs(velocity.y)));
  }
}

module.exports = CollisionDetector;
