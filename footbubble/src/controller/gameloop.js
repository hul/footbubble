var items = [];
var fps;
var time;
var element;
var isRunning;

var update = function() {
  requestAnimationFrame(update);

  var now = Date.now();
  var delta = now - time;
  time = now;

  for (var i = 0; i < items.length; i++) {
    items[i].update(delta);
  }
}

module.exports = {
  addItem: function(item) {
    items.push(item);
  },
  removeItem: function(item) {
    var index = items.indexOf(item);
    if (index != -1) items.splice(index, 1);
  },
  run: function() {
    update();
  }
}
